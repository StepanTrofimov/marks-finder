//
// Created by divaprog on 24.03.2021.
//

#include "../include/rotationMark.h"
#include <opencv2/calib3d.hpp>

namespace preprocessing {

    rotationMark::rotationMark(const cv::Mat &img, const std::vector<cv::Vec2d> &corners) :
            img(img),
            corners(corners),
            realCorners(4) {
        realCorners[0] = cv::Vec2f(0, 0);
        realCorners[1] = cv::Vec2f(0, cols);
        realCorners[2] = cv::Vec2f(rows, 0);
        realCorners[3] = cv::Vec2f(rows, cols);
    }

    cv::Mat rotationMark::solveRotImg() {
        // TODO - попробовать поменять реализацию нахождения гомографии с opencv на собственную с помощью eigen.
        H = cv::findHomography(corners, realCorners);

        cv::Mat outH;
        cv::warpPerspective(img, outH, H, {rows, cols});

        return outH.t();
    }
}