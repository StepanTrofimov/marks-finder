//
// Created by divaprog on 24.03.2021.
//

#include <opencv2/highgui.hpp>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <algorithm>

#include "iostream"
#include "../include/markCornersDetector.h"

namespace preprocessing {

    // CORNER DETECTOR
    markCornersDetector::markCornersDetector(const mat &img, const int &threshold) :
            img(img),
            threshold(threshold) {}

    void markCornersDetector::detectCornerHarris() {
        mat dst, dst_norm, dst_norm_scaled;
        cv::cornerHarris(img, dst, 2, 3, 0.04);

        cv::normalize(dst, dst_norm, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
        cv::convertScaleAbs(dst_norm, dst_norm_scaled);

        // TODO - реализовать субпиксельное уточнение координат углов

        for (int i = 0; i < dst_norm.rows; i++) {
            for (int j = 0; j < dst_norm.cols; j++) {
                if ((int) dst_norm.at<float>(i, j) > threshold)
                    corners.push_back({(double) j, (double) i});
            }
        }
    }

    std::vector<corner> preprocessing::markCornersDetector::getCorners() {
        return corners;
    }

    // EDGE DETECTOR
    markEdgeDetector::markEdgeDetector(const mat &img) :
            img(img) {}

    void markEdgeDetector::detectEdgeCanny() {
        cv::blur(img, edges, {kernel_size, kernel_size});
        cv::Canny(edges, edges, lowThreshold, lowThreshold * ratio, kernel_size);
    }


    void markEdgeDetector::detectHorizontalLinesHough() {
        double left = 0, right = img.cols;
        int lineCount = 3;
        int indxBottom = -1, indxTop = -1;
        for (int i = 0; i < 100; i++) {
            double mid = (right + left) / 2.;
            linesH.clear();
            cv::HoughLines(edges, linesH, 1, CV_PI / 180, mid, 0, 0);

            // detect centers
            std::vector<double> centers;
            for (auto line : linesH) {
                double rho = line[0], theta = line[1],
                        a = - cos(theta), b = sin(theta);
                centers.push_back((img.cols * a) / (2. + b) + rho / b);
            }
            //check count of bottom and top
            int topPoints = 0, bottomPoints = 0;
            double topCenter = 0,
                   botCenter = img.rows;
            for (int i = 0; i < centers.size(); i++) {
                if (centers[i] > img.rows / 2.) {
                    bottomPoints++;
                    if (botCenter > centers[i]) {
                        indxBottom = i;
                        botCenter = centers[i];
                    }
                }
                else {
                    topPoints++;
                    if (topCenter < centers[i]) {
                        indxTop = i;
                        topCenter = centers[i];
                    }
                }
            }
            if (bottomPoints > 2 && topPoints > 2)
                left = mid;
            else if ((topPoints == 1 && bottomPoints > 0) || (topPoints > 0 && bottomPoints == 1))
                break;
            else
                right = mid;
        }
        // Choice two lines
        if (indxTop == -1 || indxBottom == -1)
            linesH.clear();
        else {
            std::vector<cv::Vec2f> temp;
            temp.push_back(linesH[indxTop]);
            temp.push_back(linesH[indxBottom]);
            linesH.clear();
            linesH = temp;
        }
    }

    void markEdgeDetector::detectVerticalLinesHough() {
        // TODO - сделать изменяющийся порог для размера картинки
        int sizeThreshold = img.rows * 0.45;
        cv::HoughLines(edges, linesV, 1, CV_PI / 180, sizeThreshold, 0, 0);
    }

    void markEdgeDetector::tempVerticalFilter() {
        // give the Euclidean coordinates
        std::vector<cv::Vec2f> lines = linesV;
        std::vector<cv::Vec2f> cartesianLines, result;
        for (int i = 0; i < lines.size(); i++) {
            double rho = lines[i][0], theta = lines[i][1],
                    a = cos(theta), b = sin(theta);
            if ((std::abs(a / b) > -99) && ((a * rho) < img.cols * 0.2) || ((a * rho) > img.cols * 0.8))
                cartesianLines.push_back(lines[i]);
        }

        result.push_back(cartesianLines.back());
        result.push_back(cartesianLines.front());
        linesV = result;
    }

    mat markEdgeDetector::getEdges() {
        return edges;
    }

    std::vector<cv::Vec2f> markEdgeDetector::getLinesH() {
        return linesH;
    }

    std::vector<cv::Vec2f> markEdgeDetector::getLinesV() {
        return linesV;
    }

    std::vector<cv::Vec2d> markEdgeDetector::getCorners() {
        std::vector<cv::Vec2d> CornerPoints;
        // TODO - добавить тестирование о определении всех углов

        // temp
        linesV.clear();
        linesV.push_back(cv::Vec2f(1, 0));
        linesV.push_back(cv::Vec2f(img.cols - 1, 0));


        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                Eigen::Matrix2d m;
                Eigen::Vector2d rhs, point;
                m << cos(linesH[i][1]), sin(linesH[i][1]),
                     cos(linesV[j][1]), sin(linesV[j][1]);
                rhs << linesH[i][0],
                       linesV[j][0];
                point = m.inverse() * rhs;
                CornerPoints.push_back(cv::Vec2d(point.x(), point.y()));
            }
        }

        std::sort(CornerPoints.begin(), CornerPoints.end(),
                  [] (const cv::Vec2d &first, const cv::Vec2d &second) -> bool
                  {
                      return first[0] * first[1] < second[0] * second[1];
                  });

        std::swap(CornerPoints[1], CornerPoints[2]);

        return CornerPoints;
    }
}