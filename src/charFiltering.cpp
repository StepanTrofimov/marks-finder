//
// Created by divaprog on 06.04.2021.
//

#include <algorithm>
#include "charFiltering.h"

namespace postprocessing {

    double getIOU(cv::Rect_<float> bb_test, cv::Rect_<float> bb_gt) {
        float in = (bb_test & bb_gt).area();
        float un = bb_test.area() + bb_gt.area() - in;

        if (un < DBL_EPSILON)
            return 0;

        return (double)(in / un);
    }

    mark::mark(const bbox_t &bbox, const std::vector<std::string> &names) :
            x(bbox.x),
            y(bbox.y),
            w(bbox.w),
            h(bbox.h),
            prob(bbox.prob) {
        name = bbox.obj_id == INT16_MAX ? "*" : names[bbox.obj_id];
    }

    charFilterin::charFilterin(const std::vector<bbox_t> &detectRes,
                               const std::vector<std::string> &names) :
            detectResult(detectRes),
            names(names) {}

    void charFilterin::Xsort() {
        std::sort(detectResult.begin(), detectResult.end(),
                  [] (const bbox_t &a, const bbox_t &b) -> bool
                  {
                      return a.x < b.x;
                  });
    }

    void charFilterin::intersecFilter() {
        std::vector<bool> deleteIndex(detectResult.size(), false);
        // TODO - можно оптимизировать
        for (int i = 0; i < detectResult.size(); i++) {
            for (int j = i + 1; j < detectResult.size(); j++) {
                cv::Rect_<float> firstRect(detectResult[i].x, detectResult[i].y,
                                           detectResult[i].w, detectResult[i].h),
                                 secondRect(detectResult[j].x, detectResult[j].y,
                                            detectResult[j].w, detectResult[j].h);
                if (!deleteIndex[i] && (getIOU(firstRect, secondRect) > threshIoU) &&
                     detectResult[i].prob > threshProb) {
                    if (detectResult[i].prob > detectResult[j].prob)
                        deleteIndex[i] = true;
                    else
                        deleteIndex[j] = true;
                }
            }
        }
        std::vector<bbox_t> newDetectResult;
        for (int i = 0; i < detectResult.size(); i++) {
            if (!deleteIndex[i])
                newDetectResult.push_back(detectResult[i]);
        }
        detectResult = newDetectResult;
    }

    void charFilterin::meanFilter() {
        std::vector<bool> deleteIndex(detectResult.size(), false);
        for (auto iter : detectResult) {
            meanY += iter.y;
            meanH += iter.h;
        }

        meanY /= detectResult.size();
        meanH /= detectResult.size();

        for (int i = 0; i < detectResult.size(); i++) {
            if (((detectResult[i].h + 2 * detectResult[i].y) < 2 * meanY * threshSizePer) ||
                ((detectResult[i].h + 2 * detectResult[i].y) > (2 * (meanY + meanH)) * threshSizePer)) {
                deleteIndex[i] = true;
                continue;
            }
        }

        std::vector<bbox_t> newDetectResult;
        for (int i = 0; i < detectResult.size(); i++) {
            if (!deleteIndex[i])
                newDetectResult.push_back(detectResult[i]);
        }
        detectResult = newDetectResult;
    }

    void charFilterin::probFilter() {
        for (int i = 0; i < detectResult.size(); i++) {
            if (detectResult[i].prob < threshProb)
                detectResult[i].obj_id = INT16_MAX;
        }
    }

    void charFilterin::filtering() {
        Xsort();
        intersecFilter();
        meanFilter();
        probFilter();
        for (auto result : detectResult)
            filteringMarks.push_back(mark(result, names));
    }

    std::vector<mark> charFilterin::getMarks() {
        return filteringMarks;
    }
}