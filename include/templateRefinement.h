//
// Created by divaprog on 24.03.2021.
//

#ifndef MARKS_FINDER_TEMPLATEREFINEMENT_H
#define MARKS_FINDER_TEMPLATEREFINEMENT_H

#include <vector>

namespace postprocessing {

    struct Mark {

    };

    using Templates = std::vector<Mark>;

    // Loss functions
    double ProbAndIntersec();

    // Choice functions
    double TrivialChoice();

    // Class with template loss and choice functions for find mark number
    class templateRefinement {
    private:
        Templates templates;
        double threshold;
    public:
        explicit templateRefinement(const Templates &templates, const double &threshold);

        template <class loss, class choice>
        Mark detectMark(const Mark &mark, loss LossFunctor, choice ChoiceFunction) {
            std::pair<double, int> minLossAndInd({threshold, -1});
            for (int i = 0; i < templates.size(); i++) {
                double residual = loss(templates[i], mark);
                if (residual > minLossAndInd.first) {
                    minLossAndInd.first = residual;
                    minLossAndInd.second = i;
                }
            }
            // просто нашли самый наибольшее совпадающий шаблон, теперь фильтруем и вычисляем номер
            return ChoiceFunction(templates[minLossAndInd.second], mark);
        }
    };

}

#endif //MARKS_FINDER_TEMPLATEREFINEMENT_H
