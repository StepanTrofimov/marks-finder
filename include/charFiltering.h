//
// Created by divaprog on 06.04.2021.
//

#ifndef MARKS_FINDER_CHARFILTERIN_H
#define MARKS_FINDER_CHARFILTERIN_H

#include <vector>
// OpenCV идет отсюда
#include <opencv2/core.hpp>
#ifdef DARNET
#include <yolo_v2_class.hpp>
#else
struct bbox_t {
    unsigned int x, y, w, h;       // (x,y) - top-left corner, (w, h) - width & height of bounded box
    float prob;                    // confidence - probability that the object was found correctly
    unsigned int obj_id;           // class of object - from range [0, classes-1]
    unsigned int track_id;         // tracking id for video (0 - untracked, 1 - inf - tracked object)
    unsigned int frames_counter;   // counter of frames on which the object was detected
    float x_3d, y_3d, z_3d;        // center of object (in Meters) if ZED 3D Camera is used
};
#endif

namespace postprocessing {

    double getIOU(cv::Rect_<float> bb_test, cv::Rect_<float> bb_gt);

    struct mark {
        unsigned int x, y, w, h;       // (x,y) - top-left corner, (w, h) - width & height of bounded box
        float prob;                    // confidence - probability that the object was found correctly
        std::string name;

        mark(const bbox_t &bbox, const std::vector<std::string> &names);
    };

    class charFilterin {
    private:
        std::vector<bbox_t> detectResult;
        std::vector<mark> filteringMarks;

        std::vector<std::string> names;

        double meanX, meanY, meanW, meanH;

        // thresholds
        double threshIoU = 0.4,
               threshProb = 0.75,
               threshSizePer = 1.;

        void Xsort();
        void intersecFilter();
        void meanFilter();
        void probFilter();
    public:
        charFilterin(const std::vector<bbox_t> &detectRes, const std::vector<std::string> &names);
        void filtering();
        std::vector<mark> getMarks();
    };

}

#endif //MARKS_FINDER_CHARFILTERIN_H
