//
// Created by divaprog on 24.03.2021.
//

#ifndef MARKS_FINDER_ROTATIONMARK_H
#define MARKS_FINDER_ROTATIONMARK_H

#include <opencv2/imgproc.hpp>
#include <vector>

namespace preprocessing {

    class rotationMark {
    private:
        cv::Mat img, H;
        std::vector<cv::Vec2d> corners;
        std::vector<cv::Vec2d> realCorners;
        short cols = 80, rows = 20;
    public:
        explicit rotationMark(const cv::Mat &img, const std::vector<cv::Vec2d> &corners);
        cv::Mat solveRotImg();
    };

}

#endif //MARKS_FINDER_ROTATIONMARK_H
