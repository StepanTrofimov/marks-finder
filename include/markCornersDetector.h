//
// Created by divaprog on 24.03.2021.
//

#ifndef MARKS_FINDER_MARKCORNERSDETECTOR_H
#define MARKS_FINDER_MARKCORNERSDETECTOR_H

#include <opencv2/imgproc.hpp>
#include <vector>

namespace preprocessing {

    using mat = cv::Mat;
    using corner = cv::Point2d;

    class markCornersDetector {
    private:
        mat img;
        std::vector<corner> corners;
        int threshold;
    public:
        explicit markCornersDetector(const mat &img, const int &threshold = 100);
        // TODO -- разобраться с подбором параметров (возможно стоит добавить динамический подбор параметров)
        // TODO -- реализовать нахождение углов другим алгоритмов - можно попробовать sift дескрипторы.
        void detectCornerHarris();
        std::vector<corner> getCorners();
    };

    class markEdgeDetector {
    private:
        mat img;
        mat edges;
        std::vector<cv::Vec2f> linesH, linesV;
        // thresholds
        int lowThreshold = 50;
        const int max_lowThreshold = 100;
        const int ratio = 4;
        const int kernel_size = 3;
        // filters threshold
        const double pixelThreshold = 0.15;
    public:
        explicit markEdgeDetector(const mat &img);
        // detectors
        void detectEdgeCanny();
        void detectHorizontalLinesHough();
        void detectVerticalLinesHough();
        // filters
        void tempVerticalFilter();
        // get
        mat getEdges();
        std::vector<cv::Vec2f> getLinesH();
        std::vector<cv::Vec2f> getLinesV();
        std::vector<cv::Vec2d> getCorners();
    };
}

#endif //MARKS_FINDER_MARKCORNERSDETECTOR_H
