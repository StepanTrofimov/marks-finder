#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <opencv2/highgui.hpp>

#include "markCornersDetector.h"
#include "rotationMark.h"
#include "charFiltering.h"

#ifdef DARNET
#include <yolo_v2_class.hpp>
#endif

#define MARK_CORNERS_DETECION           1
#define ROTATION_MARK                   1
#define FIND_MARKS_DARKNET              0
#define CHAR_FILTERING                  0

// Darknet functions

std::vector<std::string> objects_names_from_file(std::string const filename) {
    std::ifstream file(filename);
    std::vector<std::string> file_lines;
    if (!file.is_open()) return file_lines;
    for(std::string line; file >> line;) file_lines.push_back(line);
    return file_lines;
}

int main() {
    std::string resultPath("../data/results/"),
                dataPath("../data/grz/20.jpg"), // 291 и 298 - интересные варианты
                markCornerDetecionResult("corners/"),
                rotationResult("rotation/"),
                detectionPath("classification/");
    uint cornersTime, rotationTime;
    std::chrono::steady_clock::time_point start_time, end_time;
    if (MARK_CORNERS_DETECION) {
        // reading image
        cv::Mat img = cv::imread(dataPath);

        start_time = std::chrono::steady_clock::now();

        cv::Mat grayImg;
        cv::cvtColor(img, grayImg, cv::COLOR_BGR2GRAY);

        // corner detection
        preprocessing::markCornersDetector cornDetector(grayImg);
        cornDetector.detectCornerHarris();
        std::vector<cv::Point2d> corners = cornDetector.getCorners();
        cv::Mat cornersResult = img.clone();
        for (auto corner : corners)
            cv::circle(cornersResult, corner, 1, cv::Scalar(0, 0, 255));

        cv::imwrite(resultPath + markCornerDetecionResult + "corner_detector_result.jpg", cornersResult);

        // edges detector
        preprocessing::markEdgeDetector edgDet(grayImg);
        edgDet.detectEdgeCanny();
        edgDet.detectVerticalLinesHough();
        edgDet.detectHorizontalLinesHough();
        edgDet.tempVerticalFilter(); // TODO - исправить временный фильтр
        std::vector<cv::Vec2f> Hlines = edgDet.getLinesH(),
                Vlines = edgDet.getLinesV();
        cv::Mat lineResult = img.clone(), cornersImgResult = img.clone();

        for (size_t i = 0; i < Hlines.size(); i++) {
            float rho = Hlines[i][0], theta = Hlines[i][1];
            cv::Point pt1, pt2;
            double a = cos(theta), b = sin(theta);
            double x0 = a * rho, y0 = b * rho;
            pt1.x = cvRound(x0 + 1000 * (-b));
            pt1.y = cvRound(y0 + 1000 * (a));
            pt2.x = cvRound(x0 - 1000 * (-b));
            pt2.y = cvRound(y0 - 1000 * (a));
            line(lineResult, pt1, pt2, cv::Scalar(0, 0, 255), 1, cv::LINE_AA);
        }

        cv::imwrite(resultPath + markCornerDetecionResult + "line_detector_result.jpg", lineResult);

        // Corners detection
        // Нахождение пересечения прямых, найденных ранее - углы

        std::vector<cv::Vec2d> CornerPoints = edgDet.getCorners();

        end_time = std::chrono::steady_clock::now();
        cornersTime = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();

        std::ofstream cornersPoint(resultPath + markCornerDetecionResult + "corners.txt");
        cornersPoint << CornerPoints.size() << "\n";

        for (auto point : CornerPoints) {
            cv::circle(cornersImgResult, {(int) point[0], (int) point[1]}, 3,
                       cv::Scalar(0, 0, 255), 1, cv::LINE_AA);
            cornersPoint << point[0] << " " << point[1] << "\n";
        }

        cv::imwrite(resultPath + markCornerDetecionResult + "corners_result.jpg", cornersImgResult);
    }

    if (ROTATION_MARK) {
        // find homography and transform image

        std::ifstream cornerPointsFile(resultPath + markCornerDetecionResult + "corners.txt");
        std::vector<cv::Vec2d> corners;
        int numCor;
        cornerPointsFile >> numCor;
        for (int i = 0; i < numCor; i++) {
            double tempX, tempY;
            cornerPointsFile >> tempX >> tempY;
            corners.emplace_back(tempX, tempY);
        }
        cv::Mat imgWithoutRot = cv::imread(dataPath);

        start_time = std::chrono::steady_clock::now();

        preprocessing::rotationMark rotate(imgWithoutRot, corners);
        cv::Mat imgAfterRot = rotate.solveRotImg();

        end_time = std::chrono::steady_clock::now();
        cornersTime = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();

        cv::imwrite(resultPath + rotationResult + "ready_image_fake.jpg", imgAfterRot);
    }

    std::string cfgFile("../data/darknet-data/128_64.cfg"),
            weightFile("../data/darknet-data/128_64_best.weights"),
            namesFile("../data/darknet-data/obj.names");

    auto obj_names = objects_names_from_file(namesFile);

    if (FIND_MARKS_DARKNET) {
#ifdef DARNET
        // Read img file

        cv::Mat imgDetection = cv::imread(resultPath + rotationResult + "ready_image.jpg");

        // Init darknet network

        Detector darknet(cfgFile, weightFile);
        std::vector<bbox_t> detectResult = darknet.detect(imgDetection);

        // Save result of detection

        std::ofstream detectionResultFile(resultPath + detectionPath + "char_position.txt");
        detectionResultFile << detectResult.size() << "\n";
        detectionResultFile << "x:\ty:\tw:\th:\tprob:\tchar:\n";

        for (auto iter : detectResult) {
            cv::Scalar charColor(60, 160, 260);
            cv::rectangle(imgDetection, cv::Rect(iter.x, iter.y, iter.w, iter.h), charColor, 1);
            detectionResultFile << iter.x << "\t" << iter.y << "\t" << iter.w << "\t" << iter.h << "\t" <<
                                iter.prob << "\t" << iter.obj_id << "\n";
        }

        cv::imwrite(resultPath + detectionPath + "detected_image.jpg", imgDetection);
#endif
    }

    if (CHAR_FILTERING) {
        std::ifstream detectResultFileInput(resultPath + detectionPath + "char_position.txt");
        int countChar;
        std::cin >> countChar;
        for (int i = 0; i < 6; i++) {
            std::string temp;
            detectResultFileInput >> temp;
        }

        std::vector<bbox_t> detectResult(countChar);

        for (int i = 0; i < countChar; i++) {
            bbox_t temp;
            detectResultFileInput >> temp.x >> temp.y >> temp.w >> temp.h >> temp.prob >> temp.obj_id;
        }

        postprocessing::charFilterin filter(detectResult, obj_names);
        filter.filtering();
        auto filterResult = filter.getMarks();

        std::ofstream filteringResultFile(resultPath + detectionPath + "filtering_result.txt");

        for (auto iter : filterResult) {
            filteringResultFile << iter.name << " ";
            std::cout << iter.name << " ";
        }
    }

    // TODO - поменять измерение времени в таком формате на нормальный бенчмаркинг
    std::cout << "Corners detection time:\t" << cornersTime << "\tRotation time:\t" << rotationTime << "\n";

    return 0;
}