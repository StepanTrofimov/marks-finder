//
// Created by divaprog on 01.04.2021.
//

#include <iostream>
#include <string>
#include <vector>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <algorithm>

#include "darknet.h"
#include "yolo_v2_class.hpp"

#define SIZE {80, 20}

using mat = cv::Mat;
using str = std::string;
using base = std::vector<str>;

std::vector<std::string> objects_names_from_file(std::string const filename) {
    std::ifstream file(filename);
    std::vector<std::string> file_lines;
    if (!file.is_open()) return file_lines;
    for(std::string line; file >> line;) file_lines.push_back(line);
    return file_lines;
}

// TODO - исправить не на перечисление "ручками", а через текстовый файл, например
const int classes = 2;
const base templateImg[classes] = {base{"0.jpg"},
                                   base{"1.jpg"}};

int main() {
    // TODO - сделать один хедер с указанием всех относительных путей, чтобы не прописывать их каждый раз
    str templatePath("../data/templates/"),
        imagesPath("images/"),
        charLocationPath("char_location/");

    std::vector<mat> Images;


    // init darknet network
    std::string cfgFile("../data/darknet-data/128_64.cfg"),
                weightFile("../data/darknet-data/128_64_best.weights"),
                namesFile("../data/darknet-data/obj.names");

    auto obj_names = objects_names_from_file(namesFile);

    Detector darknet(cfgFile, weightFile);

    for (int i = 0; i < classes; i++) {
        for (int j = 0; j < templateImg->size(); j++) {
            // Чтение картинки и её ресайз
            Images.clear();
            Images.resize(templateImg->size());
            Images[j] = cv::imread(templatePath + imagesPath + templateImg[i][j]);
            mat procImg;
            cv::resize(Images[j], procImg, SIZE);
            // детектирование расположение
            std::vector<bbox_t> detectResult = darknet.detect(procImg);

            // сохрание результатов
            std::ofstream detectionResultFile(templatePath + charLocationPath +
                                              templateImg[i][j] + ".txt");

            std::sort(detectResult.begin(), detectResult.end(),
                      [] (const bbox_t &a, const bbox_t &b) -> bool
            {
                return a.x < b.x;
            });

            for (auto iter : detectResult) {
                char isNum = iter.obj_id < 10 ? 'y' : 'n';
                detectionResultFile << iter.x << "\t" << iter.y << "\t" << iter.w << "\t" << iter.h
                                    << "\t" << isNum << "\n";
            }
        }
    }
}